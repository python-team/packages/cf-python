.. currentmodule:: cf
.. default-role:: obj

cf.CalendarMonths
=================

.. autoclass:: cf.CalendarMonths
   :no-members:
   :no-inherited-members:
   
CalendarMonths methods
----------------------

.. autosummary::
   :nosignatures:
   :toctree: ../generated/
   :template: method.rst

   ~cf.CalendarMonths.copy
   ~cf.CalendarMonths.inspect
   ~cf.CalendarMonths.interval
   