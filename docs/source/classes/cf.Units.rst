.. currentmodule:: cf
.. default-role:: obj

cf.Units
========

.. autoclass:: cf.Units
   :no-members:
   :no-inherited-members:

Units attributes
----------------

.. autosummary::
   :toctree: ../generated/	
   :template: attribute.rst

   ~Units.calendar
   ~Units.isdimensionless
   ~Units.islatitude
   ~Units.islongitude
   ~Units.ispressure
   ~Units.isreftime
   ~Units.istime
   ~Units.reftime
   ~Units.units

Units methods
-------------

.. autosummary::
   :nosignatures:
   :toctree: ../generated/	
   :template: method.rst

   ~cf.Units.conform
   ~cf.Units.copy
   ~cf.Units.dump
   ~cf.Units.equals
   ~cf.Units.equivalent
   ~cf.Units.formatted
   ~cf.Units.log

Units static methods
--------------------

.. autosummary::
   :nosignatures:
   :toctree: ../generated/	
   :template: method.rst

   ~Units.conform
