cf.TimeDuration
===============

.. currentmodule:: cf

.. autoclass:: TimeDuration

   
   .. automethod:: __init__

   
   .. rubric:: Methods

   .. autosummary::
   
      ~TimeDuration.__init__
      ~TimeDuration.copy
      ~TimeDuration.equals
      ~TimeDuration.equivalent
      ~TimeDuration.inspect
      ~TimeDuration.interval
      ~TimeDuration.is_day_factor
   
   

   
   
   .. rubric:: Attributes

   .. autosummary::
   
      ~TimeDuration.Units
      ~TimeDuration.isint
      ~TimeDuration.iso
   
   