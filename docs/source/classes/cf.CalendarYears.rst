.. currentmodule:: cf
.. default-role:: obj

cf.CalendarYears
================

.. autoclass:: cf.CalendarYears
   :no-members:
   :no-inherited-members:
   
CalendarYears methods
---------------------

.. autosummary::
   :nosignatures:
   :toctree: ../generated/
   :template: method.rst

   ~cf.CalendarYears.copy
   ~cf.CalendarYears.inspect
   ~cf.CalendarYears.interval
   
   