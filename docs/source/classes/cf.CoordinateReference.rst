cf.CoordinateReference
======================

.. currentmodule:: cf

.. autoclass:: CoordinateReference

   
   .. automethod:: __init__

   
   .. rubric:: Methods

   .. autosummary::
   
      ~CoordinateReference.__init__
      ~CoordinateReference.canonical_units
      ~CoordinateReference.change_coord_identities
      ~CoordinateReference.clear
      ~CoordinateReference.close
      ~CoordinateReference.copy
      ~CoordinateReference.default_value
      ~CoordinateReference.dump
      ~CoordinateReference.equals
      ~CoordinateReference.equivalent
      ~CoordinateReference.get
      ~CoordinateReference.has_key
      ~CoordinateReference.identity
      ~CoordinateReference.inspect
      ~CoordinateReference.items
      ~CoordinateReference.iteritems
      ~CoordinateReference.iterkeys
      ~CoordinateReference.itervalues
      ~CoordinateReference.keys
      ~CoordinateReference.match
      ~CoordinateReference.pop
      ~CoordinateReference.popitem
      ~CoordinateReference.remove_all_coords
      ~CoordinateReference.set
      ~CoordinateReference.setcoord
      ~CoordinateReference.setdefault
      ~CoordinateReference.structural_signature
      ~CoordinateReference.update
      ~CoordinateReference.values
   
   

   
   
   .. rubric:: Attributes

   .. autosummary::
   
      ~CoordinateReference.T
      ~CoordinateReference.X
      ~CoordinateReference.Y
      ~CoordinateReference.Z
      ~CoordinateReference.hasbounds
   
   