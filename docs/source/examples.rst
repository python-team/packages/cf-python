.. currentmodule:: cf
.. default-role:: obj

Examples
========

Regriding example
-----------------

:download:`html link <examples/regrids_example_2.html>`

:download:`Download ipy nb <examples/regrids_example_2.html>`

Next example
------------

:download:`html link <examples/regrids_example_2.html>`

:download:`Download ipy nb <examples/regrids_example_2.html>`
